# Prueba técnica perfil Elasticsearch

> **Importante:** lee antes las [consideraciones generales](../../../-/tree/main) comunes a todas las pruebas de este repositorio.

## Introducción
El objetivo de esta prueba es demostrar tus capacidades para desarrollar un buscador de textos 
mediante el uso del motor de búsquedas Elasticsearch.

## Recursos
El fichero con los datos de origen sobre el que se realizarán las búsquedas lo puedes encontrar en este mismo directorio:
**datos-prueba.zip**

## Descripción de tareas
Esta prueba comprende cuatro apartados:

1. Creación de la infraestructura.
2. Importación y exportación del conjunto de datos sobre el que realizar las búsquedas Full-Text.
3. Configuración de los índices y mapeos necesarios según el conjunto de datos dado.
4. Creación de una interfaz web que contenga un cuadro de introducción de texto para introducir términos de búsqueda y
   un filtro opcional dinámico por idioma (que se pueda buscar en todos los idiomas o en un idioma en concreto).

## Detalles de cada tarea
1. Para la infraestructura necesitarás tener en ejecución una instancia de MongoDB, de Elasticsearch y Web Server. Para ello usa docker-compose.
2. El conjunto de datos de origen es una exportación de una colección en MongoDB. Primero tienes que importar esta colección en una base de datos en Mongo.

    Para acceder a estos datos tienes que crear un programa que sea capaz de leer estos datos desde MongoDB e importarlos en Elasticsearch.

    Puedes encontrar en la documentación oficial ejemplos sobre la importación y exportación

    https://docs.mongodb.com/database-tools/mongoimport/

    https://docs.mongodb.com/database-tools/mongoexport/

    > Te aconsejamos prestar especial atención a la estructura de los datos en MongoDB para que elijas la estructura adecuada en Elasticsearch. Los datos que nos importan para realizar la búsqueda están bajo la propiedad "texts" y queda a tu criterio añadir cualquier otro dato que creas interesante para realizar las búsquedas.

3. Los objetivos de las búsquedas de texto para esta prueba son los siguientes:
    1. Que sean rápidas.
    2. Que arrojen resultados relevantes para el usuario.
    3. Que las búsquedas arrojen resultados aunque no haya escrito el término entero, por ejemplo, si introduzco el término "cami" obtendría resultados de "camiseta" o "camisetas".
    4. Que las coincidencias de texto en los resultados de la búsqueda se marquen en negrita para identificarlos visualmente.
   
   Para ello debes tener muy en cuenta la estructura de los datos para crear el/los índices que creas convenientes y los mapeos de datos adecuados para cada tipo en Elasticsearch.
4. Para ver los resultados de todo lo anterior tendrías que crear una interfaz web que contenga un cuadro de introducción de texto
para realizar las búsquedas y una opción para seleccionar un idioma sobre el cual realizar la búsqueda, si no se elije un idioma buscará, por defecto, en todos ellos. El resultado de la búsqueda deberá ser inyectado en el body a medida que se va introduciendo el término de búsqueda y resaltando las coincidencias, muestra en los resultados los datos que creas más convenientes. Hay dos opciones para crear la interfaz web, ambas usando el framework CSS Bootstrap (puedes elegir cualquier opción):
    1. Una interfaz hecha con PHP y HTML (css, javascript, etc...).
    2. Una interfaz hecha solamente con HTML (sin necesidad de llamar a scripts de lado del servidor).

## Consideraciones importantes
Para poder analizar y evaluar tu prueba en las mejores condiciones asegúrate de crear un fichero README.md con las instrucciones para levantar 
un entorno como el tuyo y todos los pasos que tenemos que dar para que tu aplicación funcione en nuestros equipos de la misma manera.

¡Ánimo y a por todas!